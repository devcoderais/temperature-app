Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  
  resources :temperature_criteria
  resources :temperatures
  devise_for :users, :path => 'u'
  resources :users do
    collection do
      get 'change_state'
    end
  end

  # root "temperature_criteria#index"
  root "notifications#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end
