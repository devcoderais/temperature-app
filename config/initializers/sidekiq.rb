if Rails.env.production?

  # Sidekiq.configure_server do |config|
  # end

  # Sidekiq.configure_client do |config|
  # end
else
  Sidekiq.configure_server do |config|
    config.redis = {url: 'redis://127.0.0.1:6379/1'}
  end

  Sidekiq.configure_client do |config|
    config.redis = {url: 'redis://127.0.0.1:6379/1'}
  end
end