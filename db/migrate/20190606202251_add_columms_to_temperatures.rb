class AddColummsToTemperatures < ActiveRecord::Migration[5.2]
  def change
    add_column :temperatures, :value, :decimal
    add_column :temperatures, :unit, :string
    add_column :temperatures, :service_provider_id, :integer
  end
end
