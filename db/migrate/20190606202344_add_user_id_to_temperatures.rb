class AddUserIdToTemperatures < ActiveRecord::Migration[5.2]
  def change
    add_column :temperatures, :user_id, :integer
  end
end
