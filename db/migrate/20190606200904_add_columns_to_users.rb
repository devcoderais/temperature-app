class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :min_temperature, :decimal
    add_column :users, :max_temperature, :decimal
    add_column :users, :unit_of_temperature, :string
    add_column :users, :enabled, :boolean
  end
end
