class CreateServiceProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :service_providers do |t|
      t.string :name
      t.json :raw_response

      t.timestamps
    end
  end
end
