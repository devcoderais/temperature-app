class AddTemperatureIdToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :temperature_id, :integer
    add_index :notifications, :temperature_id
  end
end
