class CreateTemperatureCriteria < ActiveRecord::Migration[5.2]
  def change
    create_table :temperature_criteria do |t|
      t.decimal :min_temperature
      t.decimal :max_temperature
      t.string :unit_of_temperature
      t.boolean :enabled
      t.string :city_name
      t.string :country_name

      t.timestamps
    end
  end
end
