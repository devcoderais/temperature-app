class RemoveUnwantedColumnsFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :min_temperature
    remove_column :users, :max_temperature
    remove_column :users, :unit_of_temperature
    remove_column :users, :city_name
    remove_column :users, :country_name
  end
end
