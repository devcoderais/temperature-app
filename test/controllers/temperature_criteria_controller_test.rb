require 'test_helper'

class TemperatureCriteriaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @temperature_criterium = temperature_criteria(:one)
  end

  test "should get index" do
    get temperature_criteria_url
    assert_response :success
  end

  test "should get new" do
    get new_temperature_criterium_url
    assert_response :success
  end

  test "should create temperature_criterium" do
    assert_difference('TemperatureCriterium.count') do
      post temperature_criteria_url, params: { temperature_criterium: { city_name: @temperature_criterium.city_name, country_name: @temperature_criterium.country_name, enabled: @temperature_criterium.enabled, max_temperature: @temperature_criterium.max_temperature, min_temperature: @temperature_criterium.min_temperature, unit_of_temperature: @temperature_criterium.unit_of_temperature } }
    end

    assert_redirected_to temperature_criterium_url(TemperatureCriterium.last)
  end

  test "should show temperature_criterium" do
    get temperature_criterium_url(@temperature_criterium)
    assert_response :success
  end

  test "should get edit" do
    get edit_temperature_criterium_url(@temperature_criterium)
    assert_response :success
  end

  test "should update temperature_criterium" do
    patch temperature_criterium_url(@temperature_criterium), params: { temperature_criterium: { city_name: @temperature_criterium.city_name, country_name: @temperature_criterium.country_name, enabled: @temperature_criterium.enabled, max_temperature: @temperature_criterium.max_temperature, min_temperature: @temperature_criterium.min_temperature, unit_of_temperature: @temperature_criterium.unit_of_temperature } }
    assert_redirected_to temperature_criterium_url(@temperature_criterium)
  end

  test "should destroy temperature_criterium" do
    assert_difference('TemperatureCriterium.count', -1) do
      delete temperature_criterium_url(@temperature_criterium)
    end

    assert_redirected_to temperature_criteria_url
  end
end
