require "application_system_test_case"

class TemperatureCriteriaTest < ApplicationSystemTestCase
  setup do
    @temperature_criterium = temperature_criteria(:one)
  end

  test "visiting the index" do
    visit temperature_criteria_url
    assert_selector "h1", text: "Temperature Criteria"
  end

  test "creating a Temperature criterium" do
    visit temperature_criteria_url
    click_on "New Temperature Criterium"

    fill_in "City name", with: @temperature_criterium.city_name
    fill_in "Country name", with: @temperature_criterium.country_name
    check "Enabled" if @temperature_criterium.enabled
    fill_in "Max temperature", with: @temperature_criterium.max_temperature
    fill_in "Min temperature", with: @temperature_criterium.min_temperature
    fill_in "Unit of temperature", with: @temperature_criterium.unit_of_temperature
    click_on "Create Temperature criterium"

    assert_text "Temperature criterium was successfully created"
    click_on "Back"
  end

  test "updating a Temperature criterium" do
    visit temperature_criteria_url
    click_on "Edit", match: :first

    fill_in "City name", with: @temperature_criterium.city_name
    fill_in "Country name", with: @temperature_criterium.country_name
    check "Enabled" if @temperature_criterium.enabled
    fill_in "Max temperature", with: @temperature_criterium.max_temperature
    fill_in "Min temperature", with: @temperature_criterium.min_temperature
    fill_in "Unit of temperature", with: @temperature_criterium.unit_of_temperature
    click_on "Update Temperature criterium"

    assert_text "Temperature criterium was successfully updated"
    click_on "Back"
  end

  test "destroying a Temperature criterium" do
    visit temperature_criteria_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Temperature criterium was successfully destroyed"
  end
end
