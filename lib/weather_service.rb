#This model make api call to third party service providers

# Test Response
# {"coord":{"lon":-0.13,"lat":51.51},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":285.79,"pressure":1010,"humidity":47,"temp_min":283.15,"temp_max":288.71},"visibility":10000,"wind":{"speed":3.1,"deg":180},"clouds":{"all":0},"dt":1559854929,"sys":{"type":1,"id":1502,"message":0.0104,"country":"GB","sunrise":1559792760,"sunset":1559851953},"timezone":3600,"id":2643743,"name":"London","cod":200}

require "net/http"
module WeatherService

  attr_accessor :response, :response_body
  def self.api_call(user = nil)
    res = api_response(user)
    Rails.logger.info "Making API call to openweathermap.org"
    self.response_body = res.body
    self.response = JSON.parse(response_body)
  end

  private

  def self.api_response(user = nil)
    city_name = user.temperature_criterium&.city_name
    country_name = user.temperature_criterium&.country_name
    if city_name.present? && country_name.present?
      api_key = Rails.application.credentials.dig(:open_weather_map_key)
      url = URI.parse("http://api.openweathermap.org/data/2.5/weather?q=#{city_name},#{country_name}&APPID=#{api_key}")
      req = Net::HTTP::Get.new(url.to_s)
      res = Net::HTTP.start(url.host, url.port) {|http|
        http.request(req)
      }
      return res
    end
  end
end