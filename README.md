# Temperature App

This app triggers notification based on certian rules

# Output Images

https://imgur.com/a/l7or04O

## Getting Started

1) Colone repository using ``` git clone ```
2) rake migration ``` rails db:migrate```

### Prerequisites

API key for openweathermap.org. If you don't have key contact author for decryption key of ```master.key```

```
Ruby 2.5.1
```
```
Rails 5.2
```


End with an example of getting some data out of the system or using it for a little demo

## Running the tests




## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Rails](https://github.com/rails/rails) - The web framework used
* [Ruby](https://github.com/ruby/ruby) - Programming Language




## Versioning

``` GIT```

## Authors

* **Rais Ansari** - *Initial work* - [Rais](https://bitbucket.org/devcoderais)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


