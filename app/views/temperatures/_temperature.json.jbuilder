json.extract! temperature, :id, :created_at, :updated_at
json.url temperature_url(temperature, format: :json)
