json.extract! temperature_criterium, :id, :min_temperature, :max_temperature, :unit_of_temperature, :enabled, :city_name, :country_name, :created_at, :updated_at
json.url temperature_criterium_url(temperature_criterium, format: :json)
