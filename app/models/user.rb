class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :temperature_criterium

  has_many :temperature_variations, class_name: "Temperature"
  has_many :notifications, as: :notifiable


  def update_state(status)
    case status
    when true
      if !self.enabled
        self.temperature_criterium.update(enabled: true)
        Temperature.check_temperature(@user)
      end
    when false
      self.temperature_criterium.update(enabled: false) if self.enabled
    end
  end

end
