# Use latest data to process notification
# Using in-process queing (using Active job without sidekiq/delayed job/rescue as there as old data will not be using)


class Temperature < ApplicationRecord
  extend WeatherService

  belongs_to :user, optional: true
  belongs_to :service_provider, optional: true

  def self.check_temperature(user = nil)
    WeatherServiceApiWorker.new.perform(user)
  end

  protected
  def self.notify_user(weather_data, user)
    # extract temperature from api response
    # Compare with users preferred minimum and max temperature limit
    # Response has three type of temperatures
    # temp - Average temperature
    # min_temperature - Minimum Temperature
    # max_temperature - Maximum Temperature 

    # Here using avg temperature to make calculations
    temp = weather_data.dig(:main, :temp) rescue nil
    # temp  = rand(25)
    if temp.present?
      user_min_temp = user.temperature_criterium.min_temperature
      user_max_temp = user.temperature_criterium.max_temperature
      # user_min_temp = 9
      # user_max_temp = 15
      # Considering same unit of measurement everytime
      # Else convert to standard unit of measurement, be it celsius or Fahrenheit
      last_temp = user.temperature_variations.order(:created_at).last.value rescue nil
      # Show temperature
      if self.response_body.present?
        service_record = ServiceProvider.create!(name: "openweathermap.org", raw_response: response_body)
        puts "#{service_record}"
        temperature_record = user.temperature_variations.create(value: temp, unit: "celsius", service_provider_id: service_record.id)
      end
      # Special case not in the docs: User will be notified if temperature was initially above theshold and suddenly it drops down to below threshold value
      # Need to furthur improve as counter will decrese if user actually visits notification show, or marks as read


      #NOTE: Notification is not being filtered based on business logic like - 
      # remove read notification, acted upon notification hence this will lead to long list of notification
      # Need to implement infinite scrolling too

      counter = user.notifications.count
      case
        # If previous temp was witin range or outside range
      when temp < user_min_temp && last_temp >= user_min_temp
        # Need to refractor action to a new model or setup has with definition of below actions
        notification  = user.notifications.create(action: "below_threshold", temperature_id: temperature_record.id)
        NotificationRelayJob.perform_now(counter, notification, {current_user: user})
      when temp > user_max_temp && last_temp <= user_max_temp
        # Need to refractor action to a new model or setup has with definition of below actions
        notification  = user.notifications.create(action: "above_threshold", temperature_id: temperature_record.id)
        NotificationRelayJob.perform_now(counter, notification, {current_user: user})
      when (last_temp < user_min_temp || last_temp > user_max_temp) && (temp >= user_min_temp &&  temp <= user_max_temp)
        # Need to refractor action to a new model or setup has with definition of below actions
        notification  = user.notifications.create(action: "within_threshold", temperature_id: temperature_record.id)
        NotificationRelayJob.perform_now(counter, notification, {current_user: user})
      end 
    else
      Rails.logger.fatal "Unable to reach weather api service provider"
    end
  end

  def self.data_collector(user)
    weather = Temperature.new
    loop do
      if user.enabled?
        # weather_data = weather.api_call(user)
        weather_data = {}
        puts "#{weather_data}"
        # Add wait time as there is a limitation for number of API calls in the free tier
        sleep 5
      else
        break
      end
      notify_user(weather_data, user)
    end
  end

end
