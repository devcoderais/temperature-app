class Notification < ApplicationRecord
  belongs_to :user, optional: true
  # belongs_to :recipient, optional: true, class_name: "User"
  belongs_to :notifiable, optional: true, polymorphic: true

end
