class NotificationsChannel < ApplicationCable::Channel

  def subscribed
    # stream_from "some_channel"
    stream_from "notifications:#{current_user.id}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    # Rails predefined method
    stop_all_streams
  end
end
