# irb(main):028:0> NotificationRelayJob.perform_now(1, notification, {current_user: User.last})


class NotificationRelayJob < ApplicationJob

  queue_as :default

  def perform(counter, notification, options={})
    current_user = options[:current_user]
    # some_channel = "notifications:#{current_user.id}"
    ActionCable.server.broadcast "notifications:#{current_user.id}",  counter: render_counter(counter), notification: render_notification(notification)
  end

  private

  def render_counter(counter)
    ApplicationController.renderer.render(partial: 'notifications/counter', locals: { counter: counter })
  end

  def render_notification(notification)
    ApplicationController.renderer.render(partial: 'notifications/notification', locals: { notification: notification })
  end

end