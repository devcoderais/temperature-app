

App.notifications = App.cable.subscriptions.create("NotificationsChannel", {
  connected: function(data) {
    console.log(data);
    // Called when the subscription is ready for use on the server
  },
   disconnected: function(data) {
    // Called when the subscription has been terminated by the server
  },
   received: function(data) {
    console.log(data);
    // Called when there's incoming data on the websocket for this channel
    // $("#notificationList").html("Hey there");
    $("#notificationList").prepend(data.notification);
    return this.update_counter(data.counter);
  },

  update_counter: function(counter) {
    var counter, val;
    counter = $('#notification-counter');
    val = parseInt(counter.text());
    val++;
    return counter.css({
      opacity: 0
    }).text(val).css({
      top: '-10px'
    });
  }

});