class TemperatureCriteriaController < ApplicationController
  before_action :set_temperature_criterium, only: [:show, :edit, :update, :destroy]

  # GET /temperature_criteria
  # GET /temperature_criteria.json
  def index
    @user = current_user
    @temperature_criteria = [@user.temperature_criterium].compact
  end

  # GET /temperature_criteria/1
  # GET /temperature_criteria/1.json
  def show
  end

  # GET /temperature_criteria/new
  def new
    @temperature_criterium = TemperatureCriterium.new
  end

  # GET /temperature_criteria/1/edit
  def edit
  end

  # POST /temperature_criteria
  # POST /temperature_criteria.json
  def create
    @temperature_criterium = TemperatureCriterium.new(temperature_criterium_params)
    @temperature_criterium.user_id = current_user.id
    respond_to do |format|
      if @temperature_criterium.save
        format.html { redirect_to @temperature_criterium, notice: 'Temperature criterium was successfully created.' }
        format.json { render :show, status: :created, location: @temperature_criterium }
      else
        format.html { render :new }
        format.json { render json: @temperature_criterium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /temperature_criteria/1
  # PATCH/PUT /temperature_criteria/1.json
  def update
    respond_to do |format|
      if @temperature_criterium.update(temperature_criterium_params)
        format.html { redirect_to @temperature_criterium, notice: 'Temperature criterium was successfully updated.' }
        format.json { render :show, status: :ok, location: @temperature_criterium }
      else
        format.html { render :edit }
        format.json { render json: @temperature_criterium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /temperature_criteria/1
  # DELETE /temperature_criteria/1.json
  def destroy
    @temperature_criterium.destroy
    respond_to do |format|
      format.html { redirect_to temperature_criteria_url, notice: 'Temperature criterium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_temperature_criterium
      @temperature_criterium = TemperatureCriterium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def temperature_criterium_params
      params.require(:temperature_criterium).permit(:min_temperature, :max_temperature, :unit_of_temperature, :enabled, :city_name, :country_name)
    end
end
