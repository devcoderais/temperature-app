class NotificationsController < ApplicationController
  def index
    @notifications = Notification.all.reverse
    respond_to do |f|
      f.js { render layout: false, content_type: 'text/javascript' }
      f.html
    end
  end
end