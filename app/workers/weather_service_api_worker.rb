class WeatherServiceApiWorker
  include Sidekiq::Worker
  
  # Turn off retry as we need latest weather data
  sidekiq_options retry: false
  
  def perform(user= nil, options={})
    # Make API call to weather service
    Temperature.data_collector(user)
  end

end